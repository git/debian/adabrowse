-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Abstract root type for the various output producers (HTML, XML, DocBook,
--   and so on).</DL>
--
-- <!--
-- Revision History
--
--   22-JUL-2002   TW  Initial version.
--   30-MAY-2003   TW  Added the 'Is_Private' parameter for 'Open_Unit' and
--                     'Add_Child'.
--   30-JUN-2003   TW  Rewrote the indexing stuff completely.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Asis.Text;

with AD.Crossrefs;

package AD.Printers.HTML is

   pragma Elaborate_Body;

   type Printer is new AD.Printers.Printer with private;

   procedure Open_Unit
     (Self       : access Printer;
      Unit_Kind  : in     Item_Kind;
      Unit_Name  : in     Wide_String;
      Is_Private : in     Boolean;
      XRef       : in     AD.Crossrefs.Cross_Reference);

   procedure Close_Unit
     (Self : access Printer);

   procedure Write_Comment
     (Self  : access Printer;
      Lines : in     Asis.Text.Line_List);

   procedure Open_Section
     (Self    : access Printer;
      Section : in     Section_Type);

   procedure Close_Section
     (Self    : access Printer;
      Section : in     Section_Type);

   procedure Open_Item
     (Self : access Printer;
      XRef : in     AD.Crossrefs.Cross_Reference;
      Kind : in     Item_Kind   := Not_An_Item;
      Name : in     Wide_String := "");

   procedure Close_Item
     (Self    : access Printer;
      Is_Last : in     Boolean := False);

   procedure Other_Declaration
     (Self : access Printer;
      XRef : in     AD.Crossrefs.Cross_Reference;
      Text : in     String);

   procedure Open_Container
     (Self : access Printer;
      XRef : in     AD.Crossrefs.Cross_Reference;
      Kind : in     Item_Kind;
      Name : in     Wide_String := "");

   procedure Close_Container
     (Self    : access Printer;
      Is_Last : in     Boolean := False);

   procedure Add_Child
     (Self       : access Printer;
      Kind       : in     Item_Kind;
      Is_Private : in     Boolean;
      XRef       : in     AD.Crossrefs.Cross_Reference);

   procedure Add_Exception
     (Self : access Printer;
      XRef : in     AD.Crossrefs.Cross_Reference);

   procedure Type_Name
     (Self : access Printer;
      XRef : in     AD.Crossrefs.Cross_Reference);

   procedure Type_Kind
     (Self : access Printer;
      Info : in     String);

   procedure Parent_Type
     (Self : access Printer;
      XRef : in     AD.Crossrefs.Cross_Reference);

   procedure Open_Operation_List
     (Self : access Printer;
      Kind : in     Operation_Kind);

   procedure Close_Operation_List
     (Self : access Printer);

   procedure Add_Type_Operation
     (Self : access Printer;
      XRef : in     AD.Crossrefs.Cross_Reference);

   procedure Add_Private
     (Self        : access Printer;
      For_Package : in     Boolean);

   procedure Open_Anchor
     (Self : access Printer;
      XRef : in     AD.Crossrefs.Cross_Reference);

   procedure Close_Anchor
     (Self : access Printer);

   procedure Open_XRef
     (Self : access Printer;
      XRef : in     AD.Crossrefs.Cross_Reference);

   procedure Close_XRef
     (Self : access Printer);

   procedure Put_XRef
     (Self     : access Printer;
      XRef     : in     AD.Crossrefs.Cross_Reference;
      Code     : in     Boolean := True;
      Is_Index : in     Boolean := False);
   --  Open_XRef, emit XRef.Image, Close_XRef.

   procedure Inline_Error
     (Self : access Printer;
      Msg  : in     String);

   ----------------------------------------------------------------------------
   --  Basic inline elements.

   procedure Write_Keyword
     (Self : access Printer;
      S    : in     String);

   procedure Write_Literal
     (Self : access Printer;
      S    : in     String);

   procedure Write_Attribute
     (Self : access Printer;
      S    : in     String);

   procedure Write_Comment
     (Self : access Printer;
      S    : in     String);

   procedure Write
     (Self : access Printer;
      S    : in     String);

   procedure Write_Plain
     (Self : access Printer;
      S    : in     String);

   procedure Write_Code
     (Self : access Printer;
      S    : in     String);

   ----------------------------------------------------------------------------

   procedure Open_Index
     (Self      : access Printer;
      File_Name : in     String;
      Title     : in     String;
      Present   : in     Ada.Strings.Maps.Character_Set);

   procedure Close_Index
     (Self : access Printer);

   procedure XRef_Index
     (Self      : access Printer;
      File_Name : in     String;
      Title     : in     String);

   procedure Open_Char_Section
     (Self : access Printer;
      Char : in     Character);

   procedure Close_Char_Section
     (Self : access Printer);

   procedure Open_Index_Structure
     (Self : access Printer);

   procedure Close_Index_Structure
     (Self : access Printer);

   procedure Open_Index_Item
     (Self : access Printer);

   procedure Close_Index_Item
     (Self : access Printer);

   procedure Set_Index_XRef
     (Value : in String);

private

   type Blocks is array (Natural range <>) of Boolean;
   type Block_Ptr is access Blocks;

   type Printer is new AD.Printers.Real_Printer with
      record
         Container_Level   : Integer                        := -1;
         First_In_Block    : Block_Ptr;
         Type_Has_Parent   : Boolean                        := False;
         No_XRef           : Boolean                        := False;
         In_Exception      : Boolean                        := False;
         In_Type           : Boolean                        := False;
         In_Dependencies   : Boolean                        := False;
         In_Constants      : Boolean                        := False;
         In_Top_Item       : Boolean                        := False;
         In_Task           : Boolean                        := False;
         Force_Multiline   : Boolean                        := False;
         --  Index stuff   :
         Is_Unit           : Boolean                        := False;
         First_Index       : Boolean                        := False;
         Index_Chars       : Ada.Strings.Maps.Character_Set :=
           Ada.Strings.Maps.Null_Set;
         Idx_Structures    : Natural                        := 0;
         Char_Section_Open : Boolean                        := False;
      end record;

   function Get_Suffix
     (Self : in Printer)
     return String;

   procedure Finalize
     (Self : in out Printer);

end AD.Printers.HTML;

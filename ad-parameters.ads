-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Handling of the -f parameter value.</DL>
--
-- <!--
-- Revision History
--
--   02-FEB-2002   TW  First release.
--   13-MAR-2002   TW  Changed to support -f @filename.
--   26-MAR-2002   TW  Added 'Set_Unit_Name': support for krunched file names.
--   08-JUN-2003   TW  Added 'Set_Input' with a file access.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Text_IO;

package AD.Parameters is

   Input_Error : exception;

   procedure Save_Input;

   procedure Set_Input
     (File_Name : in String);
   --  If 'File_Name' starts with '@', assumes it to be a file containing the
   --  units, opens the file and set the current input unit to the first one
   --  found in the file. If the File_Name is "@-" or just "-", reads the
   --  input units from stdin. Otherwise, assumes 'File_Name' to be the name of
   --  of the unit to process.
   --
   --  In both cases, a possible path or suffix in the unit name are ignored.
   --
   --  Raises @Input_Error@ in the first case if the file cannot be opened, or
   --  doesn't contain a unit name at all.

   procedure Set_Input
     (File : in Ada.Text_IO.File_Access);
   --  Set input to come from @File@. The file will not be closed upon @Close@!
   --  Raises @Input_Error@ if the file does not contain a unit name.

   function Advance_Input
     return Boolean;
   --  Returns True if there is a next input unit (and makes it the current
   --  input unit), False if not.

   function Is_File
     return Boolean;
   --  Returns True if the input unit names come from a file.

   procedure Close;
   --  If input comes from a file, close that file if it is open (and not
   --  stdin).

   function Source_Name
     return String;
   --  Returns the current source file name, including suffix, but without
   --  path.

   function Unit_Name
     return String;
   --  Returns the source name of the current input with all '-' replaced
   --  by '.', and without suffix. If @Set_Unit_Name@ has been called,
   --  returns whatever was set there.

   procedure Set_Unit_Name
     (Name : in String);
   --  If we've figured out that a file 'x' contains a unit named 'y', and 'y'
   --  cannot be derived from 'x', we force the unit name to 'y'.

   function Path
     return String;
   --  If the current input name contained a path, it is returned here.

end AD.Parameters;

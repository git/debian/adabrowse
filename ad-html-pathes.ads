-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Handling of "pathes": prefix URLs defined for cross-references.</DL>
--
-- <!--
-- Revision History
--
--   05-FEB-2002   TW  Initial version.
--   04-JUL-2002   TW  Added exceptions to exclusion and no_xrefs.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

package AD.HTML.Pathes is

   pragma Elaborate_Body;

   procedure Add_Path
     (Key   : in String;
      Value : in String);
   --  Key must be all lower-case.

   function Get_Path
     (Unit_Name : in String)
     return String;
   --  Returns the empty string if no key matches a prefix of Unit_Name.
   --  Otherwise, returns the value of the key matching the longest prefix
   --  of 'Unit_Name'. 'Unit_Name' must be all lower-case.

end AD.HTML.Pathes;

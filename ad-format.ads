-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Formatted output of comments.</DL>
--
-- <!--
-- Revision History
--
--   29-APR-2002   TW  Initial version.
--   09-JUL-2003   TW  Added the 'Format' function for formatting inline
--                     comments (i.e. comments within code snippets).
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Asis.Text;

with AD.Filters;
with AD.Printers;

package AD.Format is

   pragma Elaborate_Body;

   procedure Enter
     (Key    : in String;
      Filter : in AD.Filters.Filter_Ref);

   procedure Format
     (Lines       : in     Asis.Text.Line_List;
      The_Printer : access AD.Printers.Printer'Class);

   function Format
     (Inline_Comment : in String)
     return String;
   --  Run the string through the filter "shortcut | plain".

end AD.Format;

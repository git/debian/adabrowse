-------------------------------------------------------------------------------
--
-- <STRONG>Copyright (c) 2001, 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This unit is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
-- <BLOCKQUOTE>
--   As a special exception from the GPL, if other files instantiate generics
--   from this unit, or you link this unit with other files to produce an
--   executable, this unit does not by itself cause the resulting executable
--   to be covered by the GPL. This exception does not however invalidate any
--   other reasons why the executable file might be covered by the GPL.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Root package for GAL's "raw ADT" subsystem. The "raw ADTs" are used in the
--   <CODE><A HREF="gal-containers.html">Containers</A></CODE> subsystem to
--   implement containers.</DL>
--
-- <!--
-- Revision History
--
--   27-OCT-2001   TW  Initial version.
--   16-NOV-2001   TW  Added boiler-plate comments.
--   20-NOV-2001   TW  Added Unordered_Error.
-- -->
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

package GAL.ADT is

   pragma Pure;

   Container_Empty  : exception;
   --  May be raised by some ADTs when an illegal operation is attempted on
   --  an empty ADT object, e.g. trying to delete an item from an empty tree
   --  or list.

   Container_Full   : exception;
   --  May be raised by bounded ADTs when an item should be added to an ADT
   --  that is already full.

   Range_Error      : exception;
   --  Raised by operations that support ranges of some kind if the range is
   --  invalid, e.g. there are some list operations where ranges can be
   --  specified using two positions -- these operations raise Range_Error
   --  if the two positions are not on the same list, or one of them is null.

   Not_Found        : exception;
   --  Raised if an item we looked for in an ADT was not found, and the ADT
   --  object is not empty. (If it is empty, Container_Empty should be raised
   --  instead.)

   Duplicate_Key    : exception;
   --  Raised by asome ADTs that don't allow duplicates when a second item
   --  that is considered equal to an item already in the ADT is about to be
   --  added.

   Container_Error  : exception;
   --  General consistency error. Raised e.g. when an iterator is used that
   --  has becomes invalid because the item it was referencing has vanished.

   Navigation_Error : exception;
   --  Raised when a semantically illegal operation through a position is
   --  attempted, such as inserting an element after a position that already
   --  is beyond the end of a list.

   Unordered_Error  : exception;
   --  Raised by relational operators (<,>,<=,>=) if the two objects to be
   --  compared are unordered. This can e.g. happen for list positions, if
   --  the two positions are on different lists, or if one of them is null.

   --  In addition to these exceptions, any ADT may potentially raise
   --
   --  Constraint_Error   if an operation through a null-iterator is attempted.
   --  Storage_Error      if a dynamic storage allocation has failed.

end GAL.ADT;

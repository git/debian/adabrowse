-------------------------------------------------------------------------------
--
-- <STRONG>Copyright (c) 2001, 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This unit is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
-- <BLOCKQUOTE>
--   As a special exception from the GPL, if other files instantiate generics
--   from this unit, or you link this unit with other files to produce an
--   executable, this unit does not by itself cause the resulting executable
--   to be covered by the GPL. This exception does not however invalidate any
--   other reasons why the executable file might be covered by the GPL.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Signature package for simplifying using pools together with
--   containers.</DL>
--
-- <!--
-- Revision History
--
--   27-NOV-2001   TW  Initial version.
--   28-APR-2003   TW  Changed from a pure signature package to a package that
--                     actually declares an object to work around a visibility
--                     bug in GNAT 3.16a.
-- -->
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

with System.Storage_Pools;
generic
   type Manager is
     new System.Storage_Pools.Root_Storage_Pool with private;
   --  Pool : in out Manager;
package GAL.Storage.Memory is
   --  This once was a pure signature package (it had only generic formals),
   --  but due to a visibility bug in GNAT 3.16a, I changed it to only take
   --  the pool type as a parameter and then declare an object of this type.

   Pool : aliased Manager;

end GAL.Storage.Memory;



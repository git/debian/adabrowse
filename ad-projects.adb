-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Project file management.</DL>
--
-- <!--
-- Revision History
--
--   07-JUN-2003   TW  Initial version
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with AD.Projects.Impl;

package body AD.Projects is

   procedure Handle_Project_File
     (Name : in String)
     renames AD.Projects.Impl.Handle_Project_File;

   procedure Get_Source_File_List
     (File : in out Ada.Text_IO.File_Type)
     renames AD.Projects.Impl.Get_Source_File_List;

   function Get_Tree_Directory
     return String
     renames AD.Projects.Impl.Get_Tree_Directory;

   function Get_Output_Directory
     return String
     renames AD.Projects.Impl.Get_Output_Directory;

   function Get_Project_File_Name
     return String
     renames AD.Projects.Impl.Get_Project_File_Name;

   function Project_Version
     return String
     renames AD.Projects.Impl.Project_Version;

   procedure Reset
     (On_Error : in Boolean)
     renames AD.Projects.Impl.Reset;

   procedure Define_Variable
     (Name  : in String;
      Value : in String)
     renames AD.Projects.Impl.Define_Variable;

   procedure Initialize
     renames AD.Projects.Impl.Initialize;

end AD.Projects;



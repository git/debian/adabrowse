-------------------------------------------------------------------------------
--
--  <STRONG>Copyright &copy; 2001, 2002 by Thomas Wolf.</STRONG>
--  <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This software is distributed in the hope that it will
--    be useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
--  </BLOCKQUOTE>
--  <BLOCKQUOTE>
--    As a special exception from the GPL, if other files instantiate generics
--    from this unit, or you link this unit with other files to produce an
--    executable, this unit does not by itself cause the resulting executable
--    to be covered by the GPL. This exception does not however invalidate any
--    other reasons why the executable file might be covered by the GPL.
--  </BLOCKQUOTE>
--
--  <AUTHOR>
--    Thomas Wolf  (TW) <E_MAIL>
--  </AUTHOR>
--
--  <PURPOSE>
--    Operations on environment variables.
--  </PURPOSE>
--
--  <TASKING>
--    Fully task- and abortion-safe.
--  </TASKING>
--
--  <NO_STORAGE>
--
--  <HISTORY>
--    03-OCT-2001   TW  Initial version.
--    01-MAY-2002   TW  Added 'Expand'.
--    03-MAY-2002   TW  Added 'Safe_Get', and the 'Expander' types.
--  </HISTORY>
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

package Util.Environment is

   pragma Elaborate_Body;

   Not_Defined : exception;

   function Get (Name : in String) return String;
   --  Raises @Not_Defined@ if the environment variable @Name@ is not defined.
   --  Returns an empty string if the environment variable is so defined.
   --
   --  Note: it is entirely implementation-defined whether or not environment
   --  variable names are case sensitive.

   function Safe_Get
     (Name : in String)
     return String;
   --  As @Get@, but return an empty string if the variable is not defined.
   --  Never raises @Not_Defined@.

   ----------------------------------------------------------------------------

   type Expander is abstract tagged limited private;

   function Get
     (Self : access Expander;
      Name : in     String)
     return String;
   --  Default calls @Safe_Get@ above.

   type String_Expander is abstract new Expander with private;

   function Expand
     (Self   : access String_Expander;
      Source : in     String)
     return String
     is abstract;
   --  Replaces all references to environment variables in @Source@ by that
   --  variable's definition and returns the resulting string. It is undefined
   --  what happens with syntactically invalid references or references to
   --  variables that are not defined.

private

   type Expander        is abstract tagged limited null record;

   type String_Expander is abstract new Expander with null record;

end Util.Environment;



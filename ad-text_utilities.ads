-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Miscellaneous text utilities.</DL>
--
-- <!--
-- Revision History
--
--   02-FEB-2002   TW  First release.
--   07-FEB-2002   TW  Added 'Quote'.
--   06-JUN-2003   TW  Added 'To_Lower' and 'To_Upper'.
--   11-JUN-2003   TW  Added 'Canonical'.
--   22-JUL-2003   TW  Removed 'To_Lower' and 'To_Upper'.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Characters.Handling;

package AD.Text_Utilities is

   pragma Elaborate_Body;

   function To_String
     (S          : in Wide_String;
      Substitute : in Character := ' ')
     return String
     renames Ada.Characters.Handling.To_String;

   function To_Wide_String
     (S : in String)
     return Wide_String
     renames Ada.Characters.Handling.To_Wide_String;

   function Canonical
     (Suspicious_Name : in String)
     return String;
   --  Canonicalized the file name to make sure that all \ or / are replaced
   --  by the current directory separator.

   function To_File_Name
     (Unit_Name : in String;
      Suffix    : in String := "ads")
     return String;
   --  Replaces all '.' in Unit_Name by '-', maps the whole thing to lower
   --  case, appends '.' & Suffix, and returns the resulting string.

   function Quote
     (S : in String)
     return String;
   --  If 'S' does not contain white space nor single or double quotes,
   --  returns 'S'. Otherwise, encloses 'S' in double quotes and inserts
   --  a backslash ('\') before any double quote within 'S'.

end AD.Text_Utilities;

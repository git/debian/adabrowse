-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Calling the compiler.</DL>
--
-- <!--
-- Revision History
--
--   08-JUN-2003   TW  Initial version.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Strings.Unbounded;

package AD.Compiler is

   pragma Elaborate_Body;

   procedure Set_Compile_Command
     (Cmd : in String);

   function Get_Compile_Command
     return String;

   function Is_GNAT
     return Boolean;

   procedure Create_Unit
     (Name      : in     String;
      Options   : in     Ada.Strings.Unbounded.Unbounded_String;
      Tree_Dirs : in     Ada.Strings.Unbounded.Unbounded_String;
      File_Name : in out Ada.Strings.Unbounded.Unbounded_String;
      Ok        :    out Boolean);
   --  If ASIS couldn't find the given unit in the context, we may try to
   --  generate it. This is intended for ASIS-for-GNAT, where we try to call
   --  gcc to generate a tree file. For other ASIS implementations, do what-
   --  ever makes sense. If nothing can be done, just return Ok = False. If
   --  a file is created by this call, @File_Name@ should return the name of
   --  that file. @Name@ may contain a path component.

end AD.Compiler;

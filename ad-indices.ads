------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Managing indices.</DL>
--
-- <!--
-- Revision History
--
--   27-JUN-2003   TW  Initial version.
--   09-JUL-2003   TW  Added the 'Empty' field to 'Index'.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Finalization;
with Ada.Strings.Maps;
with Ada.Strings.Unbounded;

with AD.Crossrefs;
with AD.Expressions;
with AD.Printers;

with GAL.Containers.Simple;
with GAL.Storage.Standard;

with Asis;

pragma Elaborate_All (GAL.Containers.Simple);

package AD.Indices is

   pragma Elaborate_Body;

   procedure Disable;

   procedure Verify;

   procedure Add
     (Element    : in Asis.Element;
      XRef       : in AD.Crossrefs.Cross_Reference;
      Is_Private : in Boolean := False);

   function Has_Indices
     return Boolean;

   procedure Write
     (Printer : access AD.Printers.Printer'Class);

private

   type Index_Item;
   type Index_Item_Ptr is access all Index_Item;
   type Index_Item is
      record
         XRef       : AD.Crossrefs.Cross_Reference;
         Is_Private : Boolean := False;
      end record;

   package Index_Items is
      new GAL.Containers.Simple (Index_Item, GAL.Storage.Standard);

   type Item_Type is
     (Units, Packages, Types, Procedures, Functions, Entrys,
      Exceptions, Constants, Variables,
      Pragmas, Clauses);

   type Contents is array (Item_Type) of Boolean;
   pragma Pack (Contents);

   type Index is new Ada.Finalization.Limited_Controlled with
      record
         Ref_Count  : Natural                        := 1;
         File_Name  : Ada.Strings.Unbounded.Unbounded_String;
         Title      : Ada.Strings.Unbounded.Unbounded_String;
         Empty      : Ada.Strings.Unbounded.Unbounded_String;
         Rule       : AD.Expressions.Expression;
         Structured : Boolean                        := False;
         Items      : Index_Items.Simple_Container;
         Contained  : Contents                       := (others => False);
         Present    : Ada.Strings.Maps.Character_Set :=
           Ada.Strings.Maps.Null_Set;
      end record;

   type Index_Ptr is access all Index'Class;

   procedure Finalize (Idx : in out Index);

   procedure Set_Empty
     (Idx  : access Index;
      Text : in     String);

   procedure Set_Title
     (Idx   : access Index;
      Title : in     String);

   procedure Set_File_Name
     (Idx  : access Index;
      Name : in     String);

   procedure Set_Rule
     (Idx  : access Index;
      Rule : in     AD.Expressions.Expression);

   procedure Set_Structured
     (Idx  : access Index;
      Flag : in     Boolean);

   procedure Add
     (Idx        : access Index;
      Element    : in     Asis.Element;
      XRef       : in     AD.Crossrefs.Cross_Reference;
      Is_Private : in     Boolean := False);

   procedure Write
     (Idx     : access Index;
      Printer : access AD.Printers.Printer'Class;
      Name    : in     String);

   function Get
     (Name : in String)
     return Index_Ptr;

   function Find
     (Name : in String)
     return Index_Ptr;

end AD.Indices;

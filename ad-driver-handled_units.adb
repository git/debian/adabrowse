-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Global repository of all units processed.</DL>
--
-- <!--
-- Revision History
--
--   24-APR-2002   TW  Initial version.
--   11-JUN-2003   TW  Renamed to 'Handled_Units' (was 'All_Units' before)
--   13-JUN-2003   TW  It's now a separate package of AD.Driver.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with GAL.Containers.Hash_Tables;
with GAL.Storage.Standard;
with GAL.Support.Hashing;

with Util.Strings;

pragma Elaborate_All (GAL.Containers.Hash_Tables);

separate (AD.Driver)
package body Handled_Units is

   package Hashing is
      new GAL.Containers.Hash_Tables
            (Item         => String,
             Memory       => GAL.Storage.Standard,
             Initial_Size => 101,
             Hash         => GAL.Support.Hashing.Hash,
             "="          => "=");

   Repository : Hashing.Hash_Table;

   procedure Add
     (Name : in String)
   is
   begin
      Hashing.Insert (Repository, Util.Strings.To_Lower (Name));
   end Add;

   function Exists
     (Name : in String)
     return Boolean
   is
   begin
      return Hashing.Contains (Repository, Util.Strings.To_Lower (Name));
   end Exists;

begin
   Hashing.Set_Resize (Repository, 0.75);
   declare
      Linear_Growth : GAL.Support.Hashing.Linear_Growth_Policy (100);
   begin
      Hashing.Set_Growth_Policy (Repository, Linear_Growth);
   end;
end Handled_Units;

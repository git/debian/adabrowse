-------------------------------------------------------------------------------
--
--  <STRONG>Copyright &copy; 2001, 2002 by Thomas Wolf.</STRONG>
--  <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This software is distributed in the hope that it will
--    be useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
--  </BLOCKQUOTE>
--  <BLOCKQUOTE>
--    As a special exception from the GPL, if other files instantiate generics
--    from this unit, or you link this unit with other files to produce an
--    executable, this unit does not by itself cause the resulting executable
--    to be covered by the GPL. This exception does not however invalidate any
--    other reasons why the executable file might be covered by the GPL.
--  </BLOCKQUOTE>
--
--  <VERSION ID="DEVELOP">
--
--  <AUTHOR>
--    Thomas Wolf  (TW) <E_MAIL>
--  </AUTHOR>
--
--  <PURPOSE>
--    Simple operations to execute a command in the OS environment.
--    Implemented by calling the ISO-C standard function "@system@".
--
--    Both versions of @Execute@ should be considered potentially blocking
--    calls.
--
--    Also provided is an interface to the ISO C standard function "@exit@",
--    which should kill the process.
--  </PURPOSE>
--
--  <NOT_TASK_SAFE>
--
--  <NO_STORAGE>
--
--  <HISTORY>
--    21-MAR-2002   TW  Initial version.
--    25-OCT-2002   TW  Added @Terminate_Process@.
--  </HISTORY>
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

package Util.Execution is

   pragma Elaborate_Body;

   function Execute (Command : in String) return Integer;
   --  Return whatever the underlying "@system@" returns. The semantics of
   --  this return value is completely implementation-defined (in the ISO
   --  C standard). However, typical implementations return zero upon success,
   --  and a non-zero value upon failure.

   procedure Execute (Command : in String);
   --  Use this if you don't care about the return value of "@system@".

   procedure Terminate_Process
     (Status : in Integer := 0);
   --  ISO C's @exit@ function.

end Util.Execution;



-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Keeps a list of known filename/unitname pairs.</DL>
--
-- <!--
-- Revision History
--
--   09-JUL-2003   TW  Initial version
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Strings.Unbounded;

package AD.Known_Units is

   pragma Elaborate_Body;

   procedure Add
     (File_Name : in String;
      Unit_Name : in String);

   procedure Find
     (Given_File_Name  : in     String;
      Stored_File_Name :    out Ada.Strings.Unbounded.Unbounded_String;
      Stored_Path      :    out Ada.Strings.Unbounded.Unbounded_String;
      Unit_Name        :    out Ada.Strings.Unbounded.Unbounded_String);
   --  First tries the @Given_File_Name@ as is. If that fails, tries
   --  with extension ".ads", if that fails, too, tries without extension.

end AD.Known_Units;

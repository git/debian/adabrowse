-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Provides routines operating on files. Used to insulate the rest of
--   AdaBrowse from OS-specifics.</DL>
--
-- <!--
-- Revision History
--
--   04-FEB-2002   TW  Initial version for AdaBrowse 1.01.
--   19-MAR-2002   TW  'Create_Unit' accepts now a name with a path component.
--                     New operation 'Is_Absolute_Path', and 'Path' now also
--                     returns the final directory separator.
--   08-JUN-2003   TW  Moved 'Create_Unit' to package AD.Compiler.
--   19-NOV-2003   TW  Added 'Is_Directory' and 'Last_Modified'.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.IO_Exceptions;
with Ada.Strings.Unbounded;
with Ada.Text_IO;

with GNAT.OS_Lib;

package AD.File_Ops is

   pragma Elaborate_Body;

   Name_Error : exception renames Ada.IO_Exceptions.Name_Error;

   procedure Delete (Name : in String);
   --  If a file with the given @Name@ exists, try to delete it. If a failure
   --  occurs, it is swallowed! If no such file exists, nothing happens.
   --  @Name@ may contain a path component.

   function Exists (Name : in String)
     return Boolean;
   --  Returns @True@ if a file with the given @Name@ exists. @Name@ may
   --  contain a path component.

   function  Find
     (Name    : in String;
      Options : in Ada.Strings.Unbounded.Unbounded_String)
     return String;
   --  Returns the full name of the file specified by @Name@. @Options@ should
   --  be a string of "-I" options giving pathes on which to look. Returns an
   --  empty string if the source cannot be found.

   procedure Create_Unique_File
     (File      :    out Ada.Text_IO.File_Type;
      Name      :    out Ada.Strings.Unbounded.Unbounded_String;
      Base_Name : in     String;
      Extension : in     String);
   --  Creates a file havcing a unique name starting with @Base_Name@, and
   --  having the given @Extension@. @Base_Name@ should not contain an
   --  extension, but it may contain a path. @Extension@ should not contain
   --  a leading period.
   --
   --  Returns the file (opened for writing) and its full name if successful.
   --  If no file could be openend, @Name@ is empty.

   type Time_Stamp is private;
   --  Whatever the OS uses to represent times. Since it is absolutely unclear
   --  what this might be (local time or UTC, what accuracy?), there are
   --  <EM>no</EM> conversions to or from other time types. To determine
   --  whether a file is new, create a unique file, get its time stamp, delete
   --  it. Then invoke whatever is supposed to create the new file, get
   --  the new file's time stamp, and compare the two time stamps. And use
   --  "<=" because the file system might have a low time resolution!

   function "<"  (Left, Right : Time_Stamp) return Boolean;
   function "<=" (Left, Right : Time_Stamp) return Boolean;
   function ">"  (Left, Right : Time_Stamp) return Boolean;
   function ">=" (Left, Right : Time_Stamp) return Boolean;

   function Is_Directory
     (Name : in String)
     return Boolean;
   --  Returns @True@ if @Name@ refers to a directory and @False@ otherwise.

   function Last_Modified
     (Name : in String)
     return Time_Stamp;
   --  Returns an OS-dependent representation of the time the file or directory
   --  denoted by @Name@ was last modified. Raises @Name_Error@ if @Name@ does
   --  not refer to any existing file or directory.

private

   type Time_Stamp is new GNAT.OS_Lib.OS_Time;

end AD.File_Ops;

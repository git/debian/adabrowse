-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Miscellaneous text utilities.</DL>
--
-- <!--
-- Revision History
--
--   02-FEB-2002   TW  First release.
--   07-FEB-2002   TW  Added 'Quote'.
--   06-JUN-2003   TW  Added 'To_Lower' and 'To_Upper'.
--   11-JUN-2003   TW  Added 'Canonical'.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Strings.Fixed;
with Ada.Strings.Maps;
with Util.Pathes;
with Util.Strings;

pragma Elaborate_All (Util.Pathes);

package body AD.Text_Utilities is

   package ASF renames Ada.Strings.Fixed;
   package ASM renames Ada.Strings.Maps;

   use Util.Strings;

   ----------------------------------------------------------------------------

   To_Dir_Sep : constant ASM.Character_Mapping :=
     ASM.To_Mapping
       ("\/",
        Util.Pathes.Directory_Separator & Util.Pathes.Directory_Separator);
   --  Map both, so that this works on Unix and Windows!

   function Canonical
     (Suspicious_Name : in String)
     return String
   is
   begin
      return ASF.Translate (Suspicious_Name, To_Dir_Sep);
   end Canonical;

   ----------------------------------------------------------------------------

   function To_File_Name
     (Unit_Name : in String;
      Suffix    : in String := "ads")
     return String
   is
      Result : String (1 .. Unit_Name'Length + 1 + Suffix'Length);
      I      : Positive := 1;
   begin
      for J in Unit_Name'Range loop
         if Unit_Name (J) = '.' then
            Result (I) := '-';
         else
            Result (I) := To_Lower (Unit_Name (J));
         end if;
         I := I + 1;
      end loop;
      if Suffix'Last < Suffix'First then
         return Result (1 .. I - 1);
      end if;
      Result (I) := '.';
      Result (I + 1 .. Result'Last) := Suffix;
      return Result;
   end To_File_Name;

   ----------------------------------------------------------------------------

   function Quote
     (S : in String)
     return String
   is
      I : constant Natural := ASF.Index (S, Blanks);
   begin
      if I = 0 then return S; end if; --  No white space
      declare
         Result : String (1 .. I - S'First + 2 * (S'Last - I + 1) + 2);
         --  Maximum needed length.
         K      : Natural := 2;
      begin
         Result (1) := '"';
         for J in S'Range loop
            if S (J) = '"' then
               Result (K) := '\'; K := K + 1;
            end if;
            Result (K) := S (J); K := K + 1;
         end loop;
         Result (K) := '"';
         return Result (1 .. K);
      end;
   end Quote;

end AD.Text_Utilities;

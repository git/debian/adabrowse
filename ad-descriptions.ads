-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Storage of description definitions.</DL>
--
-- <!--
-- Revision History
--
--   06-FEB-2002   TW  Initial version.
--   11-NOV-2002   TW  Added 'Item_Context_Clause'.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Asis.Text;

with Asis2.Spans;

package AD.Descriptions is

   pragma Elaborate_Body;

   procedure Parse
     (Selector : in String;
      Value    : in String);

   ----------------------------------------------------------------------------

   type Item_Classes is
     (No_Item_Class,
      Item_Context_Clause,            --  with, use, or use type clause
      Item_Clause,                    --  An interior use or use type clause
      Item_Constant,
      Item_Container,                 --  Task, Protected, Package
      Item_Exception,
      Item_Instantiation,
      Item_Library,                   --  Package, Renaming, Subprogram
      Item_Library_Instantiation,
      Item_Library_Package,
      Item_Library_Renaming,
      Item_Library_Subprogram,
      Item_Object,
      Item_Package,                   --  *Nested* packages, not library level!
      Item_Pragma,
      Item_Protected,                 --  Protected objects and types
      Item_Renaming,
      Item_Rep_Clause,
      Item_Subprogram,
      Item_Task,                      --  Single_Task_Decls and task types
      Item_Type);

   function Item_Class
     (Item : in Asis.Element)
     return Item_Classes;

   function Is_Container
     (Class : in Item_Classes)
     return Boolean;

   ----------------------------------------------------------------------------

   type Comment_Finder is private;

   procedure Find
     (Self       : in     Comment_Finder;
      Item       : in     Asis.Element;
      Span       :    out Asis.Text.Span;
      Class      : in     Item_Classes := No_Item_Class);

   procedure Find
     (Self  : in     Comment_Finder;
      Item  : in     Asis.Element;
      From  : in     Asis2.Spans.Position;
      To    : in     Asis2.Spans.Position;
      Span  :    out Asis.Text.Span;
      Class : in     Item_Classes := No_Item_Class);

   procedure Clear_Comments;

   type Finders     is array (Positive range <>) of Comment_Finder;
   type Finders_Ptr is access all Finders;

   function Get_Finders
     (The_Class : in Item_Classes)
     return Finders_Ptr;

private

   type Location is (None, Before, After, Inside);

   Unlimited : constant := -1;

   type Comment_Finder is
      record
         Where   : Location;
         How_Far : Integer := Unlimited;
      end record;

end AD.Descriptions;


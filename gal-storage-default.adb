-------------------------------------------------------------------------------
--
-- <STRONG>Copyright (c) 2001, 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This unit is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
-- <BLOCKQUOTE>
--   As a special exception from the GPL, if other files instantiate generics
--   from this unit, or you link this unit with other files to produce an
--   executable, this unit does not by itself cause the resulting executable
--   to be covered by the GPL. This exception does not however invalidate any
--   other reasons why the executable file might be covered by the GPL.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   A default storage pool that uses the pool of an arbitrary
--   access-to-integer type.</DL>
--
-- <DL><DT><STRONG>
-- Tasking semantics:</STRONG><DD>
--   As all pools, fully task safe.</DL>
--
-- <DL><DT><STRONG>
-- Storage semantics:</STRONG><DD>
--   It's a <EM>storage pool</EM>, so of course it does dynamic storage
--   allocations and deallocations.</DL>
--
-- <!--
-- Revision History
--
--   27-NOV-2001   TW  Initial version.
-- -->
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

package body GAL.Storage.Default is

   type Dummy_Pointer is access Integer;

   procedure Allocate
     (Pool            : in out Manager;
      Storage_Address :    out System.Address;
      Size            : in     System.Storage_Elements.Storage_Count;
      Alignment       : in     System.Storage_Elements.Storage_Count)
   is
      pragma Warnings (Off, Pool); --  silence -gnatwa
   begin
      System.Storage_Pools.Allocate
        (Dummy_Pointer'Storage_Pool,
         Storage_Address, Size, Alignment);
   end Allocate;

   procedure Deallocate
     (Pool            : in out Manager;
      Storage_Address : in     System.Address;
      Size            : in     System.Storage_Elements.Storage_Count;
      Alignment       : in     System.Storage_Elements.Storage_Count)
   is
      pragma Warnings (Off, Pool); --  silence -gnatwa
   begin
      System.Storage_Pools.Deallocate
        (Dummy_Pointer'Storage_Pool,
         Storage_Address, Size, Alignment);
   end Deallocate;

   function Storage_Size
     (Pool : in Manager)
     return System.Storage_Elements.Storage_Count
   is
      pragma Warnings (Off, Pool); --  silence -gnatwa
   begin
      return System.Storage_Pools.Storage_Size (Dummy_Pointer'Storage_Pool);
   end Storage_Size;

end GAL.Storage.Default;

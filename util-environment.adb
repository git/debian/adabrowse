-------------------------------------------------------------------------------
--
--  <STRONG>Copyright &copy; 2001, 2002 by Thomas Wolf.</STRONG>
--  <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This software is distributed in the hope that it will
--    be useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
--  </BLOCKQUOTE>
--  <BLOCKQUOTE>
--    As a special exception from the GPL, if other files instantiate generics
--    from this unit, or you link this unit with other files to produce an
--    executable, this unit does not by itself cause the resulting executable
--    to be covered by the GPL. This exception does not however invalidate any
--    other reasons why the executable file might be covered by the GPL.
--  </BLOCKQUOTE>
--
--  <AUTHOR>
--    Thomas Wolf  (TW) <E_MAIL>
--  </AUTHOR>
--
--  <PURPOSE>
--    Operations on environment variables.
--  </PURPOSE>
--
--  <TASKING>
--    Fully task- and abortion-safe.
--  </TASKING>
--
--  <NO_STORAGE>
--
--  <HISTORY>
--    03-OCT-2001   TW  Initial version.
--    01-MAY-2002   TW  Added 'Expand'.
--    03-MAY-2002   TW  Removed 'Expand', added tagged types and child
--                      packages.
--    27-JUN-2002   TW  Added 'Get_Default' to avoid throwing and immediately
--                      handling the 'Not_Defined' exception.
--  </HISTORY>
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

with Interfaces.C.Strings;

package body Util.Environment is

   use Interfaces.C;
   use Interfaces.C.Strings;

   ----------------------------------------------------------------------------
   --  We use the 'getenv' from the C standard library.

   function Get_Env (Name : in char_array)
     return chars_ptr;
   pragma Import (C, Get_Env, "getenv");

   protected Env is
      function Get (Name : in String)
        return String;

      function Get_Default (Name : in String)
        return String;

   end Env;

   protected body Env is

      function Get
        (Name : in String)
        return String
      is
         C : chars_ptr;
      begin
         begin
            C := Get_Env (To_C (Name));
         exception
            when others =>
               C := Null_Ptr;
         end;
         if C = Null_Ptr then raise Not_Defined; end if;
         return Value (C);
      end Get;

      function Get_Default
        (Name : in String)
        return String
      is
         C : chars_ptr;
      begin
         begin
            C := Get_Env (To_C (Name));
         exception
            when others =>
               C := Null_Ptr;
         end;
         if C = Null_Ptr then return ""; end if;
         return Value (C);
      end Get_Default;

   end Env;

   ----------------------------------------------------------------------------

   function Get
     (Name : in String)
     return String
   is
   begin
      return Env.Get (Name);
   end Get;

   function Safe_Get
     (Name : in String)
     return String
   is
   begin
      if Name'Last < Name'First then return ""; end if;
      return Env.Get_Default (Name);
   end Safe_Get;

   function Get
     (Self : access Expander;
      Name : in     String)
     return String
   is
      pragma Warnings (Off, Self); --  silence -gnatwa
   begin
      return Safe_Get (Name);
   end Get;

end Util.Environment;



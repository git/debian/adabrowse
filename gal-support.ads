-------------------------------------------------------------------------------
--
-- <STRONG>Copyright (c) 2001, 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
-- <BLOCKQUOTE>
--   As a special exception from the GPL, if other files instantiate generics
--   from this unit, or you link this unit with other files to produce an
--   executable, this unit does not by itself cause the resulting executable
--   to be covered by the GPL. This exception does not however invalidate any
--   other reasons why the executable file might be covered by the GPL.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Root package for the support subsystem, which contains many generally
--   useful operations. Most of them are generic to be as widely applicable
--   as possible.</DL>
--
-- <DL><DT><STRONG>
-- Tasking semantics:</STRONG><DD>
--   N/A. Not abortion-safe.</DL>
--
-- <DL><DT><STRONG>
-- Storage semantics:</STRONG><DD>
--   No dynamic storage allocation.</DL>
--
-- <!--
-- Revision History
--
--   24-NOV-2001   TW  Initial version.
-- -->
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

package GAL.Support is

   pragma Elaborate_Body;

   type Null_Type is null record;
   --  May be useful in some instantiations, e.g. if you want a tree of
   --  Integers only: package X is new GAL.ADT.Trees (Integer, Null_Type, ...);

   Null_Object : constant Null_Type;

   generic
      type Item is private;
   procedure Swap
     (Left, Right : in out Item);
   pragma Inline (Swap);
   --  Tmp := Left; Left := Right; Right := Tmp;

private

   Null_Object : constant Null_Type := (null record);

end GAL.Support;

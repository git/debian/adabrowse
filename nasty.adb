-----------------------------------------------------------------------------
--  This is just a quick hack to show what a command to be run by AdaBrowse
--  should NOT do: it reads from stdin, and it doesn't return.
-----------------------------------------------------------------------------

with Ada.Text_IO;

procedure Nasty is
   use Ada.Text_IO;

   Std_In : File_Access := Standard_Input;
begin
   Put_Line ("nasty starts...");
   while not End_Of_File (Std_In.all) loop
      declare
         Buf : String (1 .. 500);
         Last : Natural;
      begin
         Get_Line (Std_In.all, Buf, Last);
         Put_Line (Current_Error, "nasty read: " & Buf (1 .. Last));
      end;
   end loop;
   Put_Line ("nasty won't end.");
   loop
      null;
   end loop;
end Nasty;

-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Singled out crossreference setup from AD.Writers.</DL>
--
-- <!--
-- Revision History
--
--   20-AUG-2002   TW  Initial version.
--   06-JUN-2003   TW  Moved 'Set_Standard_Units' and 'Crossref_To_Unit' here
--                     from AD.Queries.
--   09-JUL-2003   TW  New operation 'Crossref_Special' for pragmas and rep
--                     clauses.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Strings.Wide_Unbounded;

with Asis;

with AD.Messages;

with Asis2.Spans;

package AD.Crossrefs is

   pragma Elaborate_Body;

   type Cross_Reference is
      record
         Full_Unit_Name : Ada.Strings.Wide_Unbounded.Unbounded_Wide_String;
         Image          : Ada.Strings.Wide_Unbounded.Unbounded_Wide_String;
         Position       : Asis2.Spans.Position;
         Is_Local       : Boolean;
         Is_Top_Unit    : Boolean;
         Ignore         : Boolean;
      end record;

   Null_Crossref : constant Cross_Reference;

   function Crossref_Name
     (Name      : in     Asis.Defining_Name;
      This_Unit : in     Asis.Declaration;
      Reporter  : access AD.Messages.Error_Reporter'Class)
     return Cross_Reference;

   function Crossref_Special
     (Element   : in     Asis.Element;
      This_Unit : in     Asis.Declaration)
     return Cross_Reference;

   function Crossref_Exp
     (Name      : in     Asis.Expression;
      This_Unit : in     Asis.Declaration;
      Reporter  : access AD.Messages.Error_Reporter'Class)
     return Cross_Reference;

   procedure Set_Standard_Units
     (Do_Them : in Boolean);
   --  If @Do_Them@ is @True@, AdaBrowse will generate cross-refs to standard
   --  units, too.

   function Crossref_To_Unit
     (Unit : in Asis.Compilation_Unit)
     return Boolean;
   --  Returns @True@ if cross-refs to the @Unit@ shall be created. Returns
   --  @False@ for @Standard@ and any unit excluded by a NoXRef key. If
   --  <CODE>Set_Standard_Units (True)</CODE> has been called, returns @True@
   --  for non-application units, otherwise, it'll always return @False@ for
   --  such units.

private

   Null_Crossref : constant Cross_Reference :=
     (Full_Unit_Name => Ada.Strings.Wide_Unbounded.Null_Unbounded_Wide_String,
      Image          => Ada.Strings.Wide_Unbounded.Null_Unbounded_Wide_String,
      Position       => Asis2.Spans.Nil_Position,
      Is_Local       => False,
      Is_Top_Unit    => False,
      Ignore         => True);

end AD.Crossrefs;

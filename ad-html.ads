-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   HTML output helper routines.</DL>
--
-- <!--
-- Revision History
--
--   02-FEB-2002   TW  First release.
--   04-FEB-2002   TW  Added 'Get_Compile_Command' for V1.01.
--   05-FEB-2002   TW  Moved any configuration file thing to AD.Config.
--   13-MAR-2002   TW  Added 'Header' and 'Footer' with file parameter.
--                     Also added key 'Index_XRef'.
--   14-MAR-2002   TW  Added key 'Index_Title'.
--   03-APR-2002   TW  Moved all items for indices to package AD.Indices.
--   18-JUN-2002   TW  Removed a bunch of unused operations.
--   28-JUN-2002   TW  Added function 'Attributes'.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Text_IO;

package AD.HTML is

   pragma Elaborate_Body;

   procedure Header
     (File  : in Ada.Text_IO.File_Type;
      Title : in String);

   procedure Footer
     (File : in Ada.Text_IO.File_Type);

   procedure Subtitle
     (File : in Ada.Text_IO.File_Type;
      Text : in String);

   function HTMLize
     (S             : in String;
      Keep_Entities : in Boolean := True)
     return String;

   function Find_Tag_End
     (S      : in String;
      Is_End : in Boolean := False)
     return Natural;
   --  Assuming that S starts with a legal HTML tag, finds the closing ">" of
   --  the tag. Do not use for comments! If no closing ">" can be found,
   --  returns zero.

   function Attributes
     (Source : in String)
     return String;
   --  Strips leading and trailing white space and replaces all other white
   --  space by ' '.

   function Character_Set
     return String;

   ----------------------------------------------------------------------------

   type HTML_Tag_Kind is (Before, After);

   function Get_Keyword
     (What : in HTML_Tag_Kind)
     return String;

   function Get_Attribute
     (What : in HTML_Tag_Kind)
     return String;

   function Get_Definition
     (What : in HTML_Tag_Kind)
     return String;

   function Get_Comment
     (What : in HTML_Tag_Kind)
     return String;

   function Get_Literal
     (What : in HTML_Tag_Kind)
     return String;

   ----------------------------------------------------------------------------

   procedure Set_Char_Set
     (Id : in String);

   procedure Set_Style_Sheet
     (URL : in String);

   procedure Set_Body
     (S : in String);

   procedure Set_Title
     (What : in HTML_Tag_Kind;
      S    : in String);

   procedure Set_Subtitle
     (What : in HTML_Tag_Kind;
      S    : in String);

   procedure Set_Keyword
     (What : in HTML_Tag_Kind;
      S    : in String);

   procedure Set_Attribute
     (What : in HTML_Tag_Kind;
      S    : in String);

   procedure Set_Definition
     (What : in HTML_Tag_Kind;
      S    : in String);

   procedure Set_Comment
     (What : in HTML_Tag_Kind;
      S    : in String);

   procedure Set_Literal
     (What : in HTML_Tag_Kind;
      S    : in String);

end AD.HTML;



with Test.Enum;
package Test.Enum_Use is

  use Test.Enum;
  
  type My_Enum is new Enumeration;
  
  X : constant My_Enum := C;
  
  type ZXY is new XYZ;
  
  procedure Do_Foo (X : in out ZXY) renames Do_Bar;
  
  Q : ZXY := 17 + ZXY (3);
  
end Test.Enum_Use;

with Test.Signature;
generic
   with package X_Formal is new Test.Signature (<>);
package Test.Use_Signature is

   procedure Y_Proc (A, B : in X_Formal.X);
   -- The cross-reference on <CODE>X</CODE> in this procedure
   -- declaration should go to <CODE>
   -- <A HREF="test-signature.html#2">test-signature.html#2</A></CODE>.
   -- If it doesn't, check procedure <CODE>Write_Reference</CODE> in
   -- file <CODE>ad-writers.adb</CODE> in the AdaBrowse sources.
   
end Test.Use_Signature;

package Test.PrivStuff is

   type A_Type is private;

   type B_Type;

   type B_Type is
      record
         X : Natural := 0;
      end record;

private

   type A_Type is tagged
      record
         X : B_Type;
      end record;

   type C_Type;

   type C_Type is new A_Type with null record;

   type D_Type is
      record
         X : Integer := - 1;
      end record;

   type E_Type;

end Test.PrivStuff;

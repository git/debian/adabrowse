generic
   type Item is private;
procedure Test.Swap (A, B : in out Item);
pragma Inline (Test.Swap);
pragma Elaborate_Body (Test.Swap);
--  A comment


package body Test.Signature is

  procedure X_Proc (A, B : in out X)
  is
  begin
    B := A;
  end X_Proc;
  
end Test.Signature;

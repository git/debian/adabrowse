package Test.Tasking is

  N : constant := 100_000;
  
  task type T is
  
  end T;
  
  type T_Ptr is access all T;
  
  task type T2 
     (P : T_Ptr)
  is
  
    --  This is a second task type.
    
    entry A;
    entry B (Boolean) (X : in Integer);
    
    entry C (X : in Integer;
             Y : out Natural);
             
  private
  
    pragma Storage_Size (10_000);
    -- Give it a particular stack size.
    entry D (X : integer);
    
  end T2;
  
  task type T3
    (P : access T2;
     X : Integer)
  is
  
    entry Start;
    entry Stop;
    
  end T3;
  
  A_Task_Ptr : T_Ptr;
  
  task T_Object is
  
    entry Start;
    entry Kill (Normal : in Boolean);
    
  end T_Object;
  
  protected type P is
  
    procedure A (X : in natural);
    
    function B
      (X : in integer)
      return Boolean;
      
    entry C (Boolean) (A, B, C : in T_Ptr);
    
  private
  
    Flag : Boolean := false;
    
  end P;
  
  protected Lock is
    entry Acquire;
    procedure Release;
  private
    Locked : Boolean := false;
  end Lock;
  
end Test.Tasking;

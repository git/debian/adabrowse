--  The GNAT sources do not contain this file; it is generated
--  automatically when GNAT is configured.
--
--  I don't want user's of AdaBrowse to have to actually build
--  GNAT to get project file support; it is sufficient that
--  they have the GNAT sources. Therefore I provide this dummy
--  implementation. It works fine, because AdaBrowse doesn't
--  need these default locations.

package body Sdefault is

   S1 : constant String := "";

   function Include_Dir_Default_Name return String_Ptr is
   begin
      return new String'(S1);
   end Include_Dir_Default_Name;

   function Object_Dir_Default_Name return String_Ptr is
   begin
      return new String'(S1);
   end Object_Dir_Default_Name;

   function Target_Name return String_Ptr is
   begin
      return new String'(S1);
   end Target_Name;

   function Search_Dir_Prefix return String_Ptr is
   begin
      return new String'(S1);
   end Search_Dir_Prefix;

end Sdefault;

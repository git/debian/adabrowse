-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Global storage of file names.</DL>
--
-- <!--
-- Revision History
--
--   09-AUG-2002   TW  Initial version.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Strings.Unbounded;

with Util.Pathes;

package body AD.Options is

   package ASU renames Ada.Strings.Unbounded;

   Do_Private_Parts : Boolean := False;

   procedure Set_Private_Too
     (Flag : in Boolean)
   is
   begin
      Do_Private_Parts := Flag;
   end Set_Private_Too;

   function Private_Too
     return Boolean
   is
   begin
      return Do_Private_Parts;
   end Private_Too;

   Overwrite         : Boolean       := True;
   Mode              : File_Handling := Single_File;
   Out_File, Out_Dir : ASU.Unbounded_String;

   procedure Set_Output_Name
     (Name : in String)
   is
   begin
      if Util.Pathes.Name (Name) /= "" then
         Out_File := ASU.To_Unbounded_String (Name);
      else
         Out_File := ASU.Null_Unbounded_String;
      end if;
      declare
         S : constant String := Util.Pathes.Path (Name);
      begin
         if S'Last >= S'First then
            Out_Dir := ASU.To_Unbounded_String (S);
         else
            Out_Dir := ASU.Null_Unbounded_String;
         end if;
      end;
   end Set_Output_Name;

   procedure Set_Output_Directory
     (Name : in String)
   is
   begin
      Out_Dir := ASU.To_Unbounded_String (Util.Pathes.Path (Name));
   end Set_Output_Directory;

   function Output_Name
     return String
   is
   begin
      return ASU.To_String (Out_File);
   end Output_Name;

   function Output_Directory
     return String
   is
   begin
      return ASU.To_String (Out_Dir);
   end Output_Directory;

   procedure Set_Processing_Mode
     (To : in File_Handling)
   is
   begin
      Mode := To;
   end Set_Processing_Mode;

   function Processing_Mode
     return File_Handling
   is
   begin
      return Mode;
   end Processing_Mode;

   procedure Set_Overwrite
     (Allowed : in Boolean)
   is
   begin
      Overwrite := Allowed;
   end Set_Overwrite;

   function Allow_Overwrite
     return Boolean
   is
   begin
      return Overwrite;
   end Allow_Overwrite;

end AD.Options;



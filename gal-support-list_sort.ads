-------------------------------------------------------------------------------
--
-- <STRONG>Copyright (c) 2001, 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
-- <BLOCKQUOTE>
--   As a special exception from the GPL, if other files instantiate generics
--   from this unit, or you link this unit with other files to produce an
--   executable, this unit does not by itself cause the resulting executable
--   to be covered by the GPL. This exception does not however invalidate any
--   other reasons why the executable file might be covered by the GPL.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Generic list sorting operation with <CODE>O (n*log n)</CODE> worst-case
--   run-time complexity.
--
-- <DL><DT><STRONG>
-- Tasking semantics:</STRONG><DD>
--   N/A. Not abortion-safe.</DL>
--
-- <DL><DT><STRONG>
-- Storage semantics:</STRONG><DD>
--   No dynamic storage allocation. Uses <CODE>O (log n)</CODE> stack
--   space.</DL>
--
-- <!--
-- Revision History
--
--   27-NOV-2001   TW  Initial version.
-- -->
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

generic
   type Node (<>) is limited private;
   type Node_Ptr is access all Node;
   with function  "<" (L, R : in Node_Ptr) return Boolean;

   with function  Next     (N       : in Node_Ptr) return Node_Ptr;
   with procedure Set_Next (N, Next : in Node_Ptr);

   with procedure Post_Process (List, Last : in out Node_Ptr);
procedure GAL.Support.List_Sort
  (List : in out Node_Ptr;
   Last :    out Node_Ptr);
pragma Elaborate_Body (GAL.Support.List_Sort);
--  Sorts the list 'List' into ascending order according to '<'. Returns in
--  'List' the first element of the sorted list, and in 'Last' the last one.
--
--  Calls 'Post_Process' after having sorted the list.

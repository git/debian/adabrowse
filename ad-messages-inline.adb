-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Inline output of warning and error messages.</DL>
--
-- <!--
-- Revision History
--
--   22-AUG-2002   TW  Initial version.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

package body AD.Messages.Inline is

   procedure Report_Error
     (Self : in out Error_Reporter;
      Msg  : in     String)
   is
      use type AD.Printers.Printer_Ref;
   begin
      if Self.The_Printer /= null then
         AD.Printers.Inline_Error (Self.The_Printer, Msg);
      end if;
   end Report_Error;

end AD.Messages.Inline;

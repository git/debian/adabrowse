-------------------------------------------------------------------------------
--
--  <STRONG>Copyright &copy; 2001, 2002 by Thomas Wolf.</STRONG>
--  <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This software is distributed in the hope that it will
--    be useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
--  </BLOCKQUOTE>
--  <BLOCKQUOTE>
--    As a special exception from the GPL, if other files instantiate generics
--    from this unit, or you link this unit with other files to produce an
--    executable, this unit does not by itself cause the resulting executable
--    to be covered by the GPL. This exception does not however invalidate any
--    other reasons why the executable file might be covered by the GPL.
--  </BLOCKQUOTE>
--
--  <AUTHOR>
--    Thomas Wolf  (TW) <E_MAIL>
--  </AUTHOR>
--
--  <PURPOSE>
--    Root package of the calendar subsystem. Provides a day-of-week
--    calculation for Gregorian dates.
--  </PURPOSE>
--
--  <DL><DT><STRONG>Literature:</STRONG><DD>
--  <P>See Christian Zeller; <EM>Kalender-Formeln</EM>, Acta Mathematica, vol.
--  <STRONG>9</STRONG>, pp. 131-136; Nov. 1886. (Yes, that's <EM>not</EM>
--  a typo, it really appeared in eighteen-eightysix. In fact, that paper
--  is an expanded version of an earlier one from 1883!)
--  <P>See also
--  <A HREF="http://www.merlyn.demon.co.uk/zeller-c.htm">J.R. Stockton's page
--  on Zeller's congruence</A>.</DL>
--
--  <HISTORY>
--    29-JUL-2002   TW  Initial version.
--    28-OCT-2002   TW  Added 'Split'.
--  </HISTORY>
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

with Ada.Calendar;

package Util.Calendar is

   pragma Elaborate_Body;
   --  Can't be pure, because Ada.Calendar isn't pure.

   type Weekday is (Mon, Tue, Wed, Thu, Fri, Sat, Sun);

   function Day_Of_Week
     (Year  : in Ada.Calendar.Year_Number;
      Month : in Ada.Calendar.Month_Number;
      Day   : in Ada.Calendar.Day_Number)
     return Weekday;

   function Day_Of_Week
     (Date : in Ada.Calendar.Time)
     return Weekday;
   --  Returns the day of the week. Works for all Gregorian dates. Provided
   --  here because it makes available this functionality for standard
   --  Ada.Calendar times without dragging in all the extended calendar
   --  support.

   type Hours_Type   is new Natural range 0 .. 23;
   type Minutes_Type is new Natural range 0 .. 59;
   type Seconds_Type is new Natural range 0 .. 59;

   procedure Split
     (Secs : in     Ada.Calendar.Day_Duration;
      Hrs  :    out Hours_Type;
      Min  :    out Minutes_Type;
      Sec  :    out Seconds_Type;
      Frac :    out Ada.Calendar.Day_Duration);
   --  Split a number of seconds @Secs@ into hours, minutes, seconds, and a
   --  sub-second fraction.

end Util.Calendar;

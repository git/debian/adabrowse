-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   A hack to provide a dummy function GNAT 3.16a imports from a C file
--   from the GCC sources, but which actually isn't needed in AdaBrowse.</DL>
--
-- <!--
-- Revision History
--
--   10-JUN-2003   TW  Initial version.
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Interfaces.C.Strings;

separate (AD.Projects.Impl_Yes)
package body Update_Path_Hack is

   function Update_Path
     (Path      : in Interfaces.C.Strings.chars_ptr;
      Component : in Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.Strings.chars_ptr;
   pragma Export (C, Update_Path, "update_path");

   function Update_Path
     (Path      : in Interfaces.C.Strings.chars_ptr;
      Component : in Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.Strings.chars_ptr
   is
      pragma Warnings (Off, Path);      --  silence -gnatwa
      pragma Warnings (Off, Component); --  silence -gnatwa
   begin
      return Interfaces.C.Strings.New_String ("");
   end Update_Path;

   --  More hacks (for GNAT 5.03a). This operation also comes from file
   --  "prefix.c" from the gcc sources.

   procedure Set_Std_Prefix
     (S   : in Interfaces.C.Strings.chars_ptr;
      Len : in Interfaces.C.int);
   pragma Export (C, Set_Std_Prefix, "set_std_prefix");

   procedure Set_Std_Prefix
     (S   : in Interfaces.C.Strings.chars_ptr;
      Len : in Interfaces.C.int)
   is
      pragma Warnings (Off, S);   --  silence -gnatwa
      pragma Warnings (Off, Len); --  silence -gnatwa
   begin
      null;
   end Set_Std_Prefix;

   Run_Path_Option_Ptr : Interfaces.C.Strings.chars_ptr :=
     Interfaces.C.Strings.New_String ("");
   pragma Export (C, Run_Path_Option_Ptr, "__gnat_run_path_option");

end Update_Path_Hack;



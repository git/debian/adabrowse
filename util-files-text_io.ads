-------------------------------------------------------------------------------
--
--  <STRONG>Copyright &copy; 2001, 2002 by Thomas Wolf.</STRONG>
--  <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This software is distributed in the hope that it will
--    be useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
--  </BLOCKQUOTE>
--  <BLOCKQUOTE>
--    As a special exception from the GPL, if other files instantiate generics
--    from this unit, or you link this unit with other files to produce an
--    executable, this unit does not by itself cause the resulting executable
--    to be covered by the GPL. This exception does not however invalidate any
--    other reasons why the executable file might be covered by the GPL.
--  </BLOCKQUOTE>
--
--  <AUTHOR>
--    Thomas Wolf  (TW) <E_MAIL>
--  </AUTHOR>
--
--  <PURPOSE>
--    General utilities on text files.
--  </PURPOSE>
--
--  <NOT_TASK_SAFE>
--
--  <NO_STORAGE>
--
--  <HISTORY>
--    02-MAR-2002   TW  Initial version.
--    24-JUN-2002   TW  Changes to 'Next_Line': 'Delimiters' is now a character
--                      set, and you can pass a function to skip strings.
--  </HISTORY>
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

with Ada.Text_IO;
with Ada.Strings.Maps;

with Util.Strings;

package Util.Files.Text_IO is

   pragma Elaborate_Body;

   ----------------------------------------------------------------------------

   End_Error : exception renames Ada.Text_IO.End_Error;

   ----------------------------------------------------------------------------

   procedure Open_File
     (File : in out Ada.Text_IO.File_Type;
      Mode : in     Ada.Text_IO.File_Mode;
      Name : in     String;
      Form : in     String := "");
   --  First tries to open the file; if that fails, tries to create the file.

   ----------------------------------------------------------------------------

   function Get_Line
     (File : in Ada.Text_IO.File_Type := Ada.Text_IO.Current_Input)
     return String;
   --  As @Ada.Text_IO.Get_Line@, but always returns a full line from the file.
   --  Raises @End_Error@ if an attempt to read beyond the end of the file is
   --  made.

   ----------------------------------------------------------------------------

   function Default_Skip_String
     (S     : in String;
      Delim : in Character)
     return Natural;
   --  Equivalent to Util.Strings.Skip_String (S, Delim, No_Escape).

   generic
      Suppress_Blank_Lines : in Boolean := True;
      White_Space          : in Ada.Strings.Maps.Character_Set :=
        Util.Strings.Blanks;
      Line_Continuation    : in String := "\";
      Comment_Start        : in String := "#";
      Delimiters           : in Ada.Strings.Maps.Character_Set :=
        Util.Strings.String_Quotes;
      with function Strings
             (S     : in String;
              Delim : in Character)
             return Natural is Default_Skip_String;
   function Next_Line
     (File : in Ada.Text_IO.File_Type := Ada.Text_IO.Current_Input)
     return String;
   --  Returns the next line from the @File@, suppressing comments
   --  and blank lines and handling line continuations, if so desired.
   --
   --  <STRONG>Generic Parameters:</STRONG>
   --  <TABLE BORDER=0><TR><TD VALIGN="TOP"><CODE>
   --  Suppress_Blank_Lines</CODE></TD>
   --  <TD>
   --  If True, this function will never return blank lines or lines
   --  containing only a comment. If false, empty lines will be returned;
   --  lines containing only a comment will also be returned as empty
   --  lines. A blank is defined by @White_Space@.
   --  </TD></TR><TR><TD VALIGN="TOP"><CODE>
   --  White_Space</CODE></TD>
   --  <TD>
   --  Character set used to determine what is whitespace for use with
   --  @Suppress_Blank_Lines@ and line continuations.
   --  </TD></TR><TR><TD VALIGN="TOP"><CODE>
   --  Line_Continuation</CODE></TD>
   --  <TD><P>
   --  <P>A line continuation marker must be the last non-blank stuff on a
   --  line (except if a comment follows; in this case, blanks between
   --  the line continuation marker and the beginning of the comment are
   --  allowed (and ignored)).
   --  </P><P>
   --  If @Line_Continuation@ is the empty string, no line
   --  continuation handling is done, i.e. lines are never merged.
   --  </P></TD></TR><TR><TD VALIGN="TOP"><CODE>
   --  Comment_Start</CODE></TD>
   --  <TD><P>
   --  The text that marks the beginning of a comment. If empty, no comment
   --  handling is done.
   --  </P><P>
   --  A comment is always treated as a line comment, i.e. extending
   --  up to the end of the line. Bracketed comments like C's /* ... */
   --  are not supported.
   --  </P></TD></TR><TR><TD VALIGN="TOP"><CODE>
   --  Delimiters</CODE></TD>
   --  <TD>
   --  Give the set of characters that shall be string delimiters. Line
   --  continuations and comments are valid only if not within a string.
   --  If @Delimiters@ is the empty set, no string parsing on the
   --  line occurs.
   --  </TD></TR><TR><TD VALIGN="TOP"><CODE>
   --  Strings</CODE></TD>
   --  <TD><P>
   --  Used only if @Delimiters@ is not the empty set. This function is
   --  called whenever an opening string delimiter is found with the rest of
   --  the line (including the opening delimiter) and the delimiter character.
   --  It shall return zero if no matching end delimiter is found, @S'First@
   --  if despite everything this string delimiter is not to be considered the
   --  start of a string, and the index of the closing string delimiter
   --  otherwise.
   --  </P>
   --  </TD></TR></TABLE>
   --
   --  Line continuations must come before comments if on the same
   --  line; they're not allowed within comments. I.e., something like
   --  "xxx \ # comment" is read as "xxx " and a line continuation (hence
   --  the following physical line, if any, is appended automatically),
   --  while "xxx # comment \" is read as "xxx " only.
   --
   --  Raises @End_Error@ if called at the end of @File@.
   --  If EOF is hit while skipping blank lines and comments, <EM>no</EM>
   --  exception is raised but an empty string is returned. A line
   --  continuation on the last line of the file is ignored.

end Util.Files.Text_IO;

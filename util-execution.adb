-------------------------------------------------------------------------------
--
--  <STRONG>Copyright &copy; 2001, 2002 by Thomas Wolf.</STRONG>
--  <BLOCKQUOTE>
--    This piece of software is free software; you can redistribute it and/or
--    modify it under the terms of the  GNU General Public License as published
--    by the Free Software  Foundation; either version 2, or (at your option)
--    any later version. This software is distributed in the hope that it will
--    be useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
--  </BLOCKQUOTE>
--  <BLOCKQUOTE>
--    As a special exception from the GPL, if other files instantiate generics
--    from this unit, or you link this unit with other files to produce an
--    executable, this unit does not by itself cause the resulting executable
--    to be covered by the GPL. This exception does not however invalidate any
--    other reasons why the executable file might be covered by the GPL.
--  </BLOCKQUOTE>
--
--  <AUTHOR>
--    Thomas Wolf  (TW) <E_MAIL>
--  </AUTHOR>
--
--  <PURPOSE>
--    Simple operations to execute a command in the OS environment.
--    Implemented by calling the ISO-C standard function "@system@".
--
--    Both versions of @Execute@ should be considered potentially blocking
--    calls.
--
--    Also provided is an interface to the ISO C standard function "@exit@",
--    which should kill the process.
--  </PURPOSE>
--
--  <NOT_TASK_SAFE>
--
--  <NO_STORAGE>
--
--  <HISTORY>
--    21-MAR-2002   TW  Initial version.
--    25-OCT-2002   TW  Added @Terminate_Process@.
--  </HISTORY>
-------------------------------------------------------------------------------

pragma License (Modified_GPL);

with Interfaces.C;

package body Util.Execution is

   use Interfaces.C;

   function Run (Command : in char_array) return int;
   pragma Import (C, Run, "system");

   procedure Kill (Status : int);
   pragma Import (C, Kill, "exit");

   function Execute (Command : in String) return Integer
   is
   begin
      return Integer (Run (To_C (Command)));
   end Execute;

   procedure Execute (Command : in String)
   is
      Ignore : int := Run (To_C (Command));
      pragma Warnings (Off, Ignore);
      --  Silence warning about 'Ignore' not being assigned to, but not used.
   begin
      Ignore := 0;
   end Execute;

   procedure Terminate_Process
     (Status : in Integer := 0)
   is
   begin
      Kill (int (Status));
   end Terminate_Process;

end Util.Execution;



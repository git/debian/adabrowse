-------------------------------------------------------------------------------
--
--  This file is part of AdaBrowse.
--
-- <STRONG>Copyright (c) 2002 by Thomas Wolf.</STRONG>
-- <BLOCKQUOTE>
--    AdaBrowse is free software; you can redistribute it and/or modify it
--    under the terms of the  GNU General Public License as published by the
--    Free Software  Foundation; either version 2, or (at your option) any
--    later version. AdaBrowse is distributed in the hope that it will be
--    useful, but <EM>without any warranty</EM>; without even the implied
--    warranty of <EM>merchantability or fitness for a particular purpose.</EM>
--    See the GNU General Public License for  more details. You should have
--    received a copy of the GNU General Public License with this distribution,
--    see file "<A HREF="GPL.txt">GPL.txt</A>". If not, write to the Free
--    Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--    USA.
-- </BLOCKQUOTE>
--
-- <DL><DT><STRONG>
-- Author:</STRONG><DD>
--   Thomas Wolf  (TW)
--   <ADDRESS><A HREF="mailto:twolf@acm.org">twolf@acm.org</A></ADDRESS></DL>
--
-- <DL><DT><STRONG>
-- Purpose:</STRONG><DD>
--   Configuration file management.</DL>
--
-- <!--
-- Revision History
--
--   07-JUN-2003   TW  Initial version
-- -->
-------------------------------------------------------------------------------

pragma License (GPL);

with Ada.Text_IO;

package AD.Projects is

   Project_Error : exception;

   procedure Handle_Project_File
     (Name : in String);
   --  Reads, parses, and processes a GNAT project file, then extracts the
   --  object pathes ("-T") that will later be passed on to ASIS.
   --
   --  "-T" options given on the command line to AdaBrowse <EM>always</EM
   --  take precedence over the ones obtained from a project file.
   --
   --  If it finds, in the root project file or in one of the projects it
   --  extends, a variable named "<CODE>AdaBrowse_Configurations</CODE>",
   --  which may be either a single string or a string list, it uses that
   --  variables value, interpretes the string(s) as the file names of
   --  AdaBrowse configuration files and processes each of them in the order
   --  given. (If the file names contain pathes, these are, as usual, inter-
   --  preted relative to the directory where the configuration file resides.)
   --
   --  (Note: inside configuration files, path names are relative to the
   --  current directory, i.e. the one adabrowse was started in. To use pathes
   --  relative to the configuration file location use the special environment
   --  variable $@.)
   --
   --  If it finds, in the root project file or in one of the projects it
   --  extends, a variable named "<CODE>AdaBrowse_Output</CODE>", which must
   --  have a single string as its value, it uses that variable's value as
   --  the specification of a directory (relative to the directory where the
   --  project file itself is) where the generated HTML output shall go.
   --
   --  A "-o" option on the command line may override this setting!
   --
   --  Note: the AdaBrowse driver requires either a "-f" option, or, if there
   --  is none, a "-P" options specifying a project file. If a project file is
   --  specified and no "-f" option is given, AdaBrowse will process all specs
   --  from the source files of the root project.
   --
   --  Warning: if a project file is given, the compile command is changed to
   --  "gnat compile -P<file_name>", if it starts with the default compiler
   --  name (which is "gcc" -- or "gnatgcc" on some Linux installations.)
   --
   --  The AdaBrowse currently doesn't handle the other project-related
   --  switches of GNAT tools, i.e. "-X" and "-vP". To define values for
   --  external references, use environment variables; and the verbosity
   --  level of the project manager is always set to the default.
   --
   --  If any errors occur, @Project_Error@ is raised.

   procedure Get_Source_File_List
     (File : in out Ada.Text_IO.File_Type);
   --  Writes the names of the source files to @File@.

   function Get_Tree_Directory
     return String;
   --  Returns tree directory distilled from the project file.

   function Get_Output_Directory
     return String;
   --  Returns the output directory defined in the project file, or an empty
   --  string, if none is defined.

   function Get_Project_File_Name
     return String;

   function Project_Version
     return String;
   --  Returns "p" if projects are supported, and an empty string otherwise.

   procedure Reset
     (On_Error : in Boolean);
   --  Call to close the project manager.

   procedure Define_Variable
     (Name  : in String;
      Value : in String);

   procedure Initialize;

end AD.Projects;


